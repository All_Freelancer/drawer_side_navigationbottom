import 'package:drawer_side_navigationbottom/src/ui/widgets/drawer.dart';
import 'package:drawer_side_navigationbottom/src/utils/class_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//********************************************** CLASS MAIN *************************************** */
void main() {
  ClassBuilder.registerClasses();  //REGISTER ALL CLASS INDEX IN DRAWER 
  
  runApp(MyApp());
}

//********************************************* CLASS MY APP ************************************** */
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DrawerWidget(),
    );
  }
}
//************************************************************************************************* */