import 'package:drawer_side_navigationbottom/src/ui/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kf_drawer/kf_drawer.dart';

//***************************** CLASS NAVIGATION *******************************
class Navigation extends KFDrawerContent  {

  //:::::::::::::::::::::::::: CALL STATE NAVIGATION :::::::::::::::::::::::::::
  @override
  _NavigationState createState() => _NavigationState();
}

//******************************* STATE NAVIGATION *****************************
class _NavigationState extends State<Navigation> {
  // Properties & Variables needed
  int currentTab = 0; // to keep track of active tab index
  //ScrollController _hideButtonController;
  bool _isVisible = true;
 
  final nameNavigation = [
    "Events",
    "Dashboard",
    "Profile",
    "Infomation"
  ]; 

  final List<IconData> icons = [
    Icons.list,
    Icons.dashboard,
    Icons.person,
    Icons.info
  ]; 

  List<Widget> currentScreen ; // to store nested tabs

  final PageStorageBucket bucket = PageStorageBucket();
  //Widget currentScreen = EventsPage(); // Our first view in viewport

  //*************************** STATE CONSTRUCT ****************************** */
  _NavigationState(){

    //currentScreen = initPages(); 
  }

  //****************************** INIT STATE **********************************
  @override
  void initState() {
    super.initState();
    /*
    _isVisible = true;
    _hideButtonController = new ScrollController();
    currentScreen = initPages(); 

    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
       if(_isVisible)
        setState(() {
          _isVisible = false;
          print("**** $_isVisible up");
        });
      }
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if(!_isVisible)
        setState(() {
          _isVisible = true;
          print("**** $_isVisible down");
        });
      }
    });
    */
  }

  //**************************** METHOD INIT PAGES **************************** */
  /*
  List<Widget> initPages(){
    List<Widget> pages = List<Widget>(); 

    pages.add( EventsPage(nameNavigation[0], _hideButtonController));
    pages.add( DashboardPage(nameNavigation[1]));
    pages.add( ProfilePage(nameNavigation[2]));
    pages.add( Information(nameNavigation[3]));
    
    return pages; 
  }
  */
  //*************************** WIDGET ROOT HOME ************************************
 @override
  Widget build(BuildContext context) {

    /*
    return new MaterialApp(
      home: new Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: new FloatingActionButton(
          child: new Icon(Icons.add),
          onPressed: (){},
        ),

        bottomNavigationBar: new BottomAppBar(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,

            children: <Widget>[
              new IconButton(icon: Icon(Icons.menu), onPressed: (){},), 
              new IconButton(icon: Icon(Icons.search),onPressed: (){},)
            ],
          ),
        ),
      ),

    );*/

    return SafeArea(
      child: new Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: new FloatingActionButton(
          child: new Icon(Icons.add),
          onPressed: (){},
        ),

        bottomNavigationBar: new BottomAppBar(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,

            children: <Widget>[
              //new IconButton(icon: Icon(Icons.menu), onPressed: (){},), 
              DrawerWidget.buildOpen(widget.onMenuPressed), //CLOSE AND OPEN DRAWER 
              new IconButton(icon: Icon(Icons.more_vert),onPressed: (){},)
            ],
          ),
        ),
      ),
    ); 
  }  

  /*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.grey[200],
      extendBody: true,                           // ALL AREA FOR BODY

      //::::::::::::::::::::::::: PAGE STORAGE ::::::::::::::::::::::::::::
      body: PageStorage(
        child: currentScreen[currentTab],         // FIRST FRAGMENT - SCREEN
        bucket: bucket,                           //
      ),

      //::::::::::::::::::::::: FLOATING ACTION BUTTON :::::::::::::::::::::
      floatingActionButton: Widgets.buildFloatingActionButton(context),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      //:::::::::::::::::::::::: NAVIGATION BAR ::::::::::::::::::::::::::::
      //bottomNavigationBar: buildNavigation(context)
       bottomNavigationBar: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        height: _isVisible ? 60.0 : 0.0,
        child: _isVisible ? buildNavigation(context): Container(
                color:  Colors.grey[50],
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
              ),
       ) 

    );

  }*/

  //*************************** BUILD NAVIGATION BAR ***************************
  /*
  Widget buildNavigation(context){
    return  BottomAppBar(
      color: Colors.grey[50],
      
      //:::::::::::::::::::::: SHAPE BUTTON ADD EVENT :::::::::::::::::::::::
      shape: CircularNotchedRectangle(),

      //:::::::::::::::::::::: MARGIN BYTTON ADD EVENT ::::::::::::::::::::::
      notchMargin: 10,

      //::::::::::::::::::::::::::::: CONTAINER ::::::::::::::::::::::::::::::
      child: Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            //-------------- ROW LEFT (TAB BAR ICON) -------------------
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                //.................. CREATE BUTTON DASHBOARD ............
                buildMaterialButton(context, 0), 

                //.................. CREATE BUTTON EVENT ............
                buildMaterialButton(context, 1), 

              ],
            ),

            //-------------- ROW RIGHT (TAB BAR ICON) -------------------
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //................. CREATE BUTTON PROFILE .................
                buildMaterialButton(context, 2), 

                //.................. CREATE BUTTON INFORMATION ............
                buildMaterialButton(context, 3), 

              ],
            )

          ],

        ),
      ),
    );
  }
  */
  //************************* BUILD WIDGEST MATERIAL BUTTON *********************************
  /*
  Widget buildMaterialButton (context, int indexTab){
    final size = MediaQuery.of(context).size;

    return MaterialButton(
      minWidth: size.width/5,
       //:::::::::::::::::::::::::::: CLICK INDEX TAB :::::::::::::::::::::::: 
       onPressed: () {
         setState(() {
           currentTab = indexTab;
          });
        },

        //::::::::::::::::::::::::::::: ALING VERTICAL :::::::::::::::::::::::::
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              //-------------------------- ICON ----------------------
              Icon(
                icons[indexTab],
                color: currentTab == indexTab ? AppColors.colorPrimary : Colors.grey,
             ),
             
             //--------------------------- NAME ----------------------
             Text(
               nameNavigation[indexTab], 
               style: TextStyle(
                 color: currentTab == indexTab ? AppColors.colorPrimary : Colors.grey,
                ),
              ),

            ],

          ),
        ),
      );
  }
  */
}
//*****************************************************************************