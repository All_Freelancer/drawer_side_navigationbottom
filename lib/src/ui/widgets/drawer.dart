import 'package:drawer_side_navigationbottom/src/global/const.dart';
import 'package:drawer_side_navigationbottom/src/ui/pages/settings.dart';
import 'package:drawer_side_navigationbottom/src/ui/widgets/navigation.dart';
import 'package:drawer_side_navigationbottom/src/utils/class_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kf_drawer/kf_drawer.dart';

import 'package:drawer_side_navigationbottom/src/ui/pages/auth.dart';
import 'package:drawer_side_navigationbottom/src/ui/pages/calendar.dart';
//import 'package:drawer_side_navigationbottom/src/ui/pages/main_page.dart';

//**************************************** CLASS DRAWER ********************************************** */
class DrawerWidget extends StatefulWidget {
  //Variable
  final String title;

  //********************************* CONSTRUCT ******************************* */
  DrawerWidget({Key key, this.title}) : super(key: key);
  
  //********************************* CALL STATE DRAWER ********************** */
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();

  //****************************** WIDGET OPEN AND CLOSE ************************ */
  static Widget buildOpen(onMenuPressed){
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
      
      child: Material(
        shadowColor: Colors.transparent,
        color: Colors.transparent,
        child: IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          ),
        
          onPressed: onMenuPressed,   //CLOSE AND OPEN DRAWER 
        )
      ),
    ); 
  }
}

//***************************************** STATE DRAWER ********************************************** */
class _DrawerWidgetState extends State<DrawerWidget> with TickerProviderStateMixin {
  //Variable
  KFDrawerController _drawerController;

  List<Widget> initDrawer ; // 
  
  //***************************** INIT STATE ********************************* */
  @override
  void initState() {
    super.initState();

    initDrawer = indexDrawer();          // INDEX DRAWER CREATE 

    //::::::::::::::::::::: CREATE CONTROLLER FOR INDEX DRAWER ::::::::::::::: */
    _drawerController = KFDrawerController(
      //initialPage: MainPage(),
      initialPage: ClassBuilder.fromString('MainPage'),   //INIT PAGE DEFAULT (CLASS NAME) 

      items: [
        initDrawerPages(0),       //HOME
        initDrawerPages(1),       //CALENDAR 
        initDrawerPages(2)],      //SETTING
    );
  }

  //***************************** METHODS INIT PAGES **************************** */
  List<Widget> indexDrawer(){
    List<Widget> pages = List<Widget>(); 

    //pages.add( MainPage(ConstGlobal.nameDrawer[0]));
    pages.add( Navigation());
    pages.add( CalendarPage(ConstGlobal.nameDrawer[1]));
    pages.add( SettingsPage(ConstGlobal.nameDrawer[2]));
    
    return pages; 
  }

  //******************************* METHOD INDEX DRAWER ************************** */
  KFDrawerItem initDrawerPages(int index){

      return  KFDrawerItem.initWithPage(
          text: Text(ConstGlobal.nameDrawer[index], style: TextStyle(color: Colors.white)),
          icon: Icon(ConstGlobal.iconDrawer[index], color: Colors.white),
          //page: ClassBuilder.fromString('SettingsPage'),
          page: initDrawer[index],
      );         
  }

  //********************************* WIDGET ROOT ******************************** */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: KFDrawer(
//        borderRadius: 0.0,
//        shadowBorderRadius: 0.0,
//        menuPadding: EdgeInsets.all(0.0),
//        scrollable: true,

        //:::::::::::::::::::::::::: CONTROLLER - CHANGE INDEX PAGE ::::::::::::::::
        controller: _drawerController,

        //:::::::::::::::::::::::::::::::: HEADER - PROFILE :::::::::::::::::::::::::
        header: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            width: MediaQuery.of(context).size.width * 0.6,
            child: Image.asset(
              'assets/logo.png',
              alignment: Alignment.centerLeft,
            ),
          ),
        ),

        //:::::::::::::::::::::::::::::::: FOOTER - LOGIN/LOGOUT :::::::::::::::::::::
        footer: KFDrawerItem(
          text: Text(
            'SIGN IN',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(
            Icons.input,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).push(CupertinoPageRoute(
              fullscreenDialog: true,
              builder: (BuildContext context) {
                return AuthPage("Sign in");
              },
            ));
          },
        ),

        //::::::::::::::::::::::::::::::::: THEME FOR DRAWER :::::::::::::::::::::::::
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Color.fromRGBO(255, 255, 255, 1.0), Color.fromRGBO(44, 72, 171, 1.0)],
            tileMode: TileMode.repeated,
          ),
        ),
      ),
    );
  }

}
//**************************************************************************************************** */
