import 'package:drawer_side_navigationbottom/src/ui/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';

//************************************* CLASS CALENDAR PAGE ******************************************* */
class CalendarPage extends KFDrawerContent {
  final String title; 

  //******************************** CONSTRUCT ************************************** */
  CalendarPage(this.title); 

  //***************************** CALL STATE CALENDAR PAGE ******************************** */
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

//************************************** STATE CALENDAR PAGE ****************************************** */
class _CalendarPageState extends State<CalendarPage> {

  //************************************ WIDGET ROOT ******************************** */
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                DrawerWidget.buildOpen(widget.onMenuPressed), //CLOSE AND OPEN DRAWER 
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(widget.title),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
//************************************************************************************************ */