import 'package:drawer_side_navigationbottom/src/ui/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';

//**************************************** CLASS MAIN PAGE **************************************  */
class MainPage extends KFDrawerContent {
  //Variable 
  final String title; 
  
  //********************************* CONSTRUCT ********************************* */
  MainPage(this.title, { Key key,});

  //******************************** CALL STATE MAIN PAGE ************************ */
  @override
  _MainPageState createState() => _MainPageState();
}

//*************************************** STATE MAIN PAGE *************************************** */
class _MainPageState extends State<MainPage> {

  //*********************************** WIDGET ROOT ******************************* */
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                 DrawerWidget.buildOpen(widget.onMenuPressed), //CLOSE AND OPEN DRAWER 
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(widget.title),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
//*********************************************************************************************** */