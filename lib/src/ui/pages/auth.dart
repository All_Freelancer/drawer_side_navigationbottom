import 'package:flutter/material.dart';

//************************************* CLASS AUTHPAGE ******************************************* */
class AuthPage extends StatefulWidget {
  //Variable
  final String title; 

  //******************************** CONSTRUCT ************************************** */
  AuthPage(this.title); 

  //***************************** CALL STATE AUTHPAGE ******************************** */
  @override
  _AuthPageState createState() => _AuthPageState();
}

//************************************** STATE AUTHPAGE ****************************************** */
class _AuthPageState extends State<AuthPage> {

  //************************************ WIDGET ROOT ******************************** */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SafeArea(
          child: Center(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(32.0)),
                      child: Material(
                        shadowColor: Colors.transparent,
                        color: Colors.transparent,
                        child: IconButton(
                          icon: Icon(
                            Icons.menu,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();  //TURN INDEX DRAWER BEFORE
                          },
                        ),
                      ),
                    ),
                  ],
                ),

                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.title),
                    ],
                  ),
                ),
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}
//************************************************************************************************ */