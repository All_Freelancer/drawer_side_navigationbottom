import 'package:drawer_side_navigationbottom/src/global/const.dart';
import 'package:drawer_side_navigationbottom/src/ui/pages/calendar.dart';
import 'package:drawer_side_navigationbottom/src/ui/pages/main_page.dart';
import 'package:drawer_side_navigationbottom/src/ui/pages/settings.dart';

//*********************************** TYPEDDEF ************************************** */
typedef T Constructor<T>();

final Map<String, Constructor<Object>> _constructors = <String, Constructor<Object>>{};

void register<T>(Constructor<T> constructor) {
  _constructors[T.toString()] = constructor;
}

//***************************************** CLASS BUILDER *****************************  */
class ClassBuilder {
  static void registerClasses() {

    register<MainPage>(() => MainPage(ConstGlobal.nameDrawer[0]));
    register<CalendarPage>(() => CalendarPage(ConstGlobal.nameDrawer[1]));
    register<SettingsPage>(() => SettingsPage(ConstGlobal.nameDrawer[2]));
  }

  //::::::::::::::::::: RETURN PAGE WITH INDEX TYPE ::::::::::::::::::::::::::::::::::::
  static dynamic fromString(String type) {
    return _constructors[type]();
  }
}
//****************************************************************************************** */